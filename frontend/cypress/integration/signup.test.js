describe('signup page (unlogged) test', () => {
  beforeEach(() => {
    cy.visit('/signup');
    cy.url().should('include', '/signup');
    cy.get("a[href='/signout']").should('not.be.visible');
  });

  it('should display all validations errors', () => {
    cy.get('form button').click();
    cy.get('.error')
      .should(($errors) => {
        expect($errors).to.have.length(3);
      });
  });

  it('should display password not equal error', () => {
    // get input, type into it and verify that the value has been updated
    cy.get('input[name=\'email\']')
      .type('cle@gmail.com')
      .should('have.value', 'cle@gmail.com');
    cy.get('input[name=\'password\']')
      .type('Azerty')
      .should('have.value', 'Azerty');
    cy.get('input[name=\'secondPassword\']')
      .type('Azerty123')
      .should('have.value', 'Azerty123');
    cy.get('form button').click();
    cy.get('#root > div > div.container.body_content > form > div:nth-child(4) > fieldset > span')
      .should('be.visible')
      .should(($el) => {
        expect($el).to.have.length(1);
        expect($el[0].outerText).to.contain('Les valeurs ne sont pas indentiques');
      });
  });

  it('should display email already used error', () => {
    cy.exec('npm run db:reset');
    cy.request('POST', 'http://localhost:3002/signup', {
      email: 'cle@gmail.com',
      password: 'Azerty123',
    });
    cy.get('input[name=\'email\']')
      .type('cle@gmail.com')
      .should('have.value', 'cle@gmail.com');
    cy.get('input[name=\'password\']')
      .type('Azerty123')
      .should('have.value', 'Azerty123');
    cy.get('input[name=\'secondPassword\']')
      .type('Azerty123')
      .should('have.value', 'Azerty123');
    cy.get('form button').click();
    cy.get('#root > div > div.container.body_content > div')
      .should('be.visible')
      .should(($el) => {
        expect($el).to.have.length(1);
        expect($el[0].outerText).to.contain('Email déjà utilisé');
      });
  });

  it('requires email input be to fill', () => {
    cy.get('input[name=\'email\']').focus().blur();
    cy.get('#root > div > div.container.body_content > form > div:nth-child(2) > fieldset > span')
      .should('be.visible');
  });

  it('requires password input to be fill', () => {
    cy.get('input[name=\'password\']').focus().blur();
    cy.get('#root > div > div.container.body_content > form > div:nth-child(3) > fieldset > span')
      .should('be.visible');
  });

  it('requires confirmation password input to be fill', () => {
    cy.get('input[name=\'secondPassword\']').focus().blur();
    cy.get('#root > div > div.container.body_content > form > div:nth-child(4) > fieldset > span')
      .should('be.visible');
  });
});
