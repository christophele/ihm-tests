import React from 'react';
import { shallow } from 'enzyme';
/*eslint-disable */
import Header from '../containers/Header';
import RootTest from './RootTest';
import { incrementActionCount } from '../actions';
import AuthentificationReducer from '../reducers/Authentification';
import { SET_AUTHENTIFICATION, INCREMENT_ACTION_COUNT } from '../actions/action-types';
/* eslint-enable */

/* test sur composant connecté avec redux */
describe('Test sur Header', () => {
  it('Render component without error', () => {
    // eslint-disable-next-line no-unused-vars
    const wrapper = shallow(
      <RootTest>
        <Header />
      </RootTest>,
    );
  });

  it('test action s payload', () => {
    const action = incrementActionCount();
    expect(action.type).toEqual(INCREMENT_ACTION_COUNT);
  });

  it('test Authentification reducer user is unlogged', () => {
    const initialState = {
      isLoggedIn: false,
    };
    expect(AuthentificationReducer(initialState, {}).isLoggedIn).toEqual(false);
  });

  it('test Authentification reducer / action setting user to logged', () => {
    const action = {
      type: SET_AUTHENTIFICATION,
      payload: true,
    };
    const initialState = {
      isLoggedIn: false,
    };
    expect(AuthentificationReducer(initialState, action).isLoggedIn).toEqual(true);
  });
});
