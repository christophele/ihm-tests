import React, {useState, useEffect} from "react";
import APIHelper from "../APIHelper.js"

function TodoApp() {
    const [todos, setTodos] = useState([])
    const [todo, setTodo] = useState("")
  
    useEffect(() => {
        const fetchTodoAndSetTodos = async () => {
            const todos = await APIHelper.getAllTodos()
            setTodos(todos)
        }
        fetchTodoAndSetTodos()
    }, [])
  
    const createTodo = async e => {
        e.preventDefault()
        if (!todo) {
            alert("Entrer une tâche")
            return
        }
        if (todos.some(({ task }) => task === todo)) {
            alert(`La tâche existe déjà`)
            return
        }
        const newTodo = await APIHelper.createTodo(todo)
        setTodos([...todos, newTodo])
        setTodo('')
    }
  
    const deleteTodo = async (e, id) => {
        try {
            e.stopPropagation()
            await APIHelper.deleteTodo(id)
            setTodos(todos.filter(({ _id: i }) => id !== i))
        } catch (err) {}
    }
  
    const updateTodo = async (e, id) => {
        e.stopPropagation()
        const payload = {
            completed: !todos.find(todo => todo._id === id).completed,
        }
        const updatedTodo = await APIHelper.updateTodo(id, payload)
        setTodos(todos.map(todo => (todo._id === id ? updatedTodo : todo)))
    }
  
    return (
        <React.Fragment>
            <div className="mb-10">
                <input
                    id="todo-input"
                    type="text"
                    placeholder="Nom de tâche"
                    value={todo}
                    className="mr-6 appearance-none bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                    onChange={({ target }) => setTodo(target.value)}
                />
                <button 
                    type="button"
                    id="todo-add-button"
                    onClick={createTodo}
                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
                >
                    Ajouter
                </button>
            </div>
            <table className="text-md bg-white shadow rounded mb-4 table-fixed">
                <thead>
                    <tr className="bg-gray-200 border-b">
                        <th className="text-left p-3 px-5 w-3/4">Nom tâche</th>
                        <th className="text-left p-3 px-5 w-1/2">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {todos.length > 0 ? todos.map(({ _id, task, completed }, i) => (
                        <tr key={_id} className="border-b hover:bg-orange-100 cursor-pointer transition duration-200 ease-in-out">
                            <td
                                key={i}
                                onClick={e => updateTodo(e, _id)}
                                className={`todo-item block p-3 px-5 w-3/4 ${completed ? "completed" : ""}`}
                            >
                            {task} 
                            </td>
                            <td className="p-3 px-5 w-1/2">
                                <button 
                                    onClick={e => deleteTodo(e, _id)}
                                    className="delete bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                                >
                                    Supprimer
                                </button>
                            </td>
                        </tr>
                    ))
                    : <tr><td className="text-center block p-3" colSpan={2} id="no-todo-found">Il y a actuellement 0 tâche</td></tr>
                    }
                </tbody>
            </table>
        </React.Fragment>
    );
}
  
export default TodoApp;