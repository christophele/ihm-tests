import { createSelector } from 'reselect';
import lodash from 'lodash';

/* selector = fonction qui reçoit tout le store en paramètre */
export const getIntegerList = state => state.ressources.ressourceList;

export const getContainsOneList = state => getIntegerList(state).filter(
  r => r.toString().indexOf('1') > -1,
);

const isPrimeNumber = (value) => {
  // eslint-disable-next-line no-plusplus
  for (let i = 2; i < value; i++) {
    if (value % i === 0) {
      return false;
    }
  }
  return value > 1;
};

export const getPrimeNumberList = state => getIntegerList(state).filter(r => isPrimeNumber(r));

export const getSpecialNumbersList = createSelector(
  getContainsOneList,
  getPrimeNumberList,
  (containsOneList, primeNumbersList) => lodash.intersection(containsOneList, primeNumbersList),
);
