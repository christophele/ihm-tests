// eslint-disable-next-line no-irregular-whitespace
import { incrementActionCount } from '../actions';
import { INCREMENT_ACTION_COUNT } from '../actions/action-types';

// eslint-disable-next-line import/prefer-default-export
export const ActionCounter = store => next => (action) => {
  if (action.type !== INCREMENT_ACTION_COUNT) {
    store.dispatch(incrementActionCount());
  }

  next(action);
};
