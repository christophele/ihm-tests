const mongoose = require("mongoose") // requiring the mongoose package
const todoSchema = new mongoose.Schema({
    task: {
        type: String,
        required: true
    },
    completed: {
        type: Boolean, 
        default: false
    },
})

const todoModel = mongoose.model("todo", todoSchema) // creating the model from the schema

module.exports = todoModel;