const mongoose = require('mongoose');
const { db } = require('./models/userModel');

mongoose.connect(
    'mongodb://127.0.0.1:27017/tests',
    {useNewUrlParser: true}
);

mongoose.connection.once('open', () => db.dropDatabase());

setTimeout(() => {
    process.exit();
}, 2000);
